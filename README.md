# Azure Solutions
This repository contains wrappers libraries for azure servies to be used for ease of development. 
Below are the list of azure services implemented.
* Azure Cosmos DB
  * MongoDB API
  * SQL API
* Azure Redis Cache
* Azure Service Bus
  * Topic
  * Queue
* Azure Storage Accounts
  * Blob
  * Table
  * Queue

## Note:
This code is not production ready and need to be tested before using in a production environment.
Please refer [here for license](https://github.com/JDSRAO/Azure.Solutions/blob/master/LICENSE)
